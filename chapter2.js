function changeWord(selectedText, changedText, text) {
    return text.replace(selectedText, changedText)
}

const kalimat1 = 'Andini sangat mencintai kamu selamanya'
const kalimat2 = 'Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu'

console.log("No. 1")
console.log(changeWord('mencintai', 'membenci', kalimat1))
console.log(changeWord('bromo', 'semeru', kalimat2))

function checkTypeNumber(givenNumber) {
    if (givenNumber === undefined) {
        return "Error: Bro where is the parameter?"
    }
    if (typeof (givenNumber) === "number") {
        return givenNumber % 2 === 0 ? "GENAP" : "GANJIL"
    }

    return "Error: Invalid data type"
}

console.log("\nNo. 2")
console.log(checkTypeNumber(10))
console.log(checkTypeNumber(3))
console.log(checkTypeNumber("3"))
console.log(checkTypeNumber({}))
console.log(checkTypeNumber([]))
console.log(checkTypeNumber())

function checkEmail(email) {
    let pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/
    if (email === undefined) {
        return "Error: No arguments"
    }
    if (typeof email == "string") {
        return email.includes("@") ? email.match(pattern) ? "VALID" : "INVALID" : "Error: doesn't have an \"@\" symbol"
    }

    return "Error: Invalid data type"
}

console.log("\nNo. 3")
console.log(checkEmail('apranata@binar.co.id'))
console.log(checkEmail('apranata@binar.com'))
console.log(checkEmail('apranata@binar'))
console.log(checkEmail('apranata'))
console.log(checkTypeNumber(checkEmail(3322)))
console.log(checkEmail())

function isValidPassword(givenPassword) {
    let pattern = /^(?=.{8,})(?=.*[a-z])(?=.*[A-Z]).*$/
    if (givenPassword === undefined) {
        return "Error: No arguments"
    }
    if (typeof givenPassword == "string") {
        return !!givenPassword.match(pattern)
    }

    return "Error: Invalid data type"
}

console.log("\nNo. 4")
console.log(isValidPassword('Meong2021'))
console.log(isValidPassword('meong2021'))
console.log(isValidPassword('@eong'))
console.log(isValidPassword('Meong2'))
console.log(isValidPassword(0))
console.log(isValidPassword())

function getSplitName(personName) {
    if (typeof personName === undefined) {
        return "Error: No arguments"
    }
    if (typeof personName === 'string') {
        let name = personName.split(' ')
        switch (name.length) {
            case 1 :
                return {
                    firstName: name[0],
                    middleName: null,
                    lastName: null
                }
            case 2 :
                return {
                    firstName: name[0],
                    middleName: null,
                    lastName: name[1]
                }
            case 3 :
                return {
                    firstName: name[0],
                    middleName: name[1],
                    lastName: name[2]
                }
            default :
                return "Error: This function is only for 3 words"
        }
    }

    return "Invalid data type"
}

console.log("\nNo. 5")
console.log(getSplitName('Aldi Daniela Pranata'))
console.log(getSplitName('Dwi Kuncoro'))
console.log(getSplitName('Aurora'))
console.log(getSplitName('Aurora Aureliya Sukma Darma'))
console.log(getSplitName(0))

function getAngkaTerbesarKedua(dataNumbers) {
    if (typeof dataNumbers === 'undefined') {
        return "Error: No arguments"
    }
    if (typeof dataNumbers === 'object') {
        let unique = [...new Set(dataNumbers)].sort((a, b) => b - a)
        return unique[1]
    }

    return "Error: Invalid data type"
}

const dataAngka = [9, 4, 7, 7, 4, 3, 2, 2, 8]

console.log("\nNo. 6")
console.log(getAngkaTerbesarKedua(dataAngka))
console.log(getAngkaTerbesarKedua(0))
console.log(getAngkaTerbesarKedua())

const dataPenjualanPakAldi = [
    {
        namaProduct: 'Sepatu Futsal Nike Vapor Academy 8',
        hargaSatuan: 760000,
        kategori: "Sepatu Sport",
        totalTerjual: 90,
    },
    {
        namaProduct: 'Sepatu Warrior Tristan Black Brown High',
        hargaSatuan: 960000,
        kategori: "Sepatu Sneaker",
        totalTerjual: 37,
    },
    {
        namaProduct: 'Sepatu Warrior Tristan Maroon High ',
        kategori: "Sepatu Sneaker",
        hargaSatuan: 360000,
        totalTerjual: 90,
    },
    {
        namaProduct: 'Sepatu Warrior Rainbow Tosca Corduroy',
        hargaSatuan: 120000,
        kategori: "Sepatu Sneaker",
        totalTerjual: 90,
    }
]

function hitungTotalPenjualan(dataPenjualan) {
    if (typeof dataPenjualan === 'undefined') {
        return "Error: No arguments"
    }
    if (typeof dataPenjualan === 'object') {
        return dataPenjualan.reduce((prev, curr) => prev + curr.totalTerjual, 0);
    }

    return "Error: Invalid data type"
}

console.log("\nNo. 7")
console.log(hitungTotalPenjualan(dataPenjualanPakAldi))

const dataPenjualanNovel = [
    {
        idProduct: 'BOOK002421',
        namaProduk: 'Pulang - Pergi',
        penulis: 'Tere Liye',
        hargaBeli: 60000,
        hargaJual: 86000,
        totalTerjual: 150,
        sisaStok: 17,
    },
    {
        idProduct: 'BOOK002351',
        namaProduk: 'Selamat Tinggal',
        penulis: 'Tere Liye',
        hargaBeli: 75000,
        hargaJual: 103000,
        totalTerjual: 171,
        sisaStok: 20,
    },
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Garis Waktu',
        penulis: 'Fiersa Besari',
        hargaBeli: 67000,
        hargaJual: 99000,
        totalTerjual: 213,
        sisaStok: 5,
    },
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Laskar Pelangi',
        penulis: 'Andrea Hirata',
        hargaBeli: 55000,
        hargaJual: 68000,
        totalTerjual: 20,
        sisaStok: 56,
    },
];

function konversiRupiah(uang) {
    return `Rp ${new Intl.NumberFormat("id-ID").format(uang)}`
}

function getInfoPenjualan(dataPenjualan) {
    if (typeof dataPenjualan === 'undefined') {
        return "Error: No arguments"
    }
    if (typeof dataPenjualan === 'object') {
        let totalKeuntungan = dataPenjualan.reduce((prev, curr) => prev + (curr.hargaJual - curr.hargaBeli) * curr.totalTerjual, 0)
        let totalModal = dataPenjualan.reduce((prev, curr) => prev + (curr.hargaBeli * (curr.totalTerjual + curr.sisaStok)), 0)
        let persentaseKeuntungan = (totalKeuntungan / totalModal) * 100
        let terbanyakDijual = dataPenjualan.reduce((prev, curr) => prev.totalTerjual > curr.totalTerjual ? prev : curr)
        let result = dataPenjualan.reduce((total, value) => {
            total[value.penulis] = (total[value.penulis] || 0) + value.totalTerjual
            return total
        }, {})
        let penulisTerlaris = Object.entries(result).sort(([, a], [, b]) => b - a)

        return {
            totalKeuntungan: konversiRupiah(totalKeuntungan),
            totalModal: konversiRupiah(totalModal),
            persentaseKeuntungan: `${persentaseKeuntungan.toFixed(2)}%`,
            produkBukuTerlaris: terbanyakDijual.namaProduk,
            penulisTerlaris: penulisTerlaris[0][0],
        }
    }

    return "Error: Invalid data type"
}

console.log("\nNo. 8")
console.log(getInfoPenjualan(dataPenjualanNovel))
